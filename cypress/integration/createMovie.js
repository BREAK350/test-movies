const host = 'http://localhost:3000';

describe('create movie', () => {
	beforeEach(() => {
		// login to add a video
		cy
			.window()
			.its('localStorage')
			.then(ls => ls.setItem('auth', JSON.stringify({ user: 'dmytro', password: 'password' })));
	});

	it('should create a movie', () => {
		cy.visit(`${host}/library`);
		cy.get('[data-test-id="addNewMovie"]').invoke('click');

		// fill in the form
		cy.get('[name="name"]').type('Venom');
		cy.get('[name="description"]').type('When Eddie Brock acquires the powers of a symbiote, he will have to release his alter-ego "Venom" to save his life.');
		cy.get('[name="year"]').type('2018');
		cy.get('[name="genre"]').type('Horror');
		cy.get('[name="picture"]').type('https://m.media-amazon.com/images/M/MV5BMTU3MTQyNjQwM15BMl5BanBnXkFtZTgwNDgxNDczNTM@._V1_SY1000_CR0,0,675,1000_AL_.jpg');

		cy.get('[type="submit"]').click();

		cy.get('[data-test-id="tile-Venom"]').should('not.be.empty');
	});
});