import React from 'react';
import { NextContext } from 'next';
import superagent from 'superagent';

export type Request<R> = (options: RequestOptions) => Promise<R>;

export interface Services {
	[serviceName: string]: (request: any) => any;
}

type WrapServices<T> = { [K in keyof T]: T[K] extends (...args: any[]) => any ? ReturnType<T[K]> : never };

export interface WithServicesProps<S> {
	services: WrapServices<S>;
}

export interface NextContextWithServices<S> extends NextContext, WithServicesProps<S> {}

export interface RequestOptions {
	method: 'get' | 'post' | 'patch' | 'del';
	url: string;
	query?: { [name: string]: any };
	body?: any;
}

export default function withServices<OwnProps, S extends { [name: string]: any }>(services: S) {
	function wrapServices(services: S, ctx?: NextContext): WrapServices<S> {
		function request(options: RequestOptions) {
			const req = superagent[options.method](options.url);

			const cookies = ctx && ctx.req && ctx.req.headers && ctx.req.headers.cookie;

			if (cookies) {
				// FIXME
				const cookie = Array.isArray(cookies) ? cookies[0] : cookies;
				req.set('cookie', cookie);
			}

			if (options.query) {
				req.query(options.query);
			}

			if (options.body) {
				req.send(options.body);
			}

			return req.then(({ body }) => body);
		}

		return Object.keys(services).reduce(
			(res, name) =>
				Object.assign({}, res, {
					[name]: services[name](request),
				}),
			{} as WrapServices<S>
		);
	}

	const wrappedServices = wrapServices(services);

	return (WrappedComponent: React.ComponentType<OwnProps & WithServicesProps<S>>) => {
		return class WithServices extends React.Component<OwnProps> {
			static async getInitialProps(ctx: NextContext) {
				let pageProps: any;

				if (typeof WrappedComponent.getInitialProps === 'function') {
					const nextContextWithServices: NextContextWithServices<S> = {
						...ctx,
						services: wrapServices(services, ctx),
					};
					pageProps = await WrappedComponent.getInitialProps(nextContextWithServices);
				}

				return pageProps;
			}

			render() {
				return <WrappedComponent {...this.props} services={wrappedServices} />;
			}
		};
	};
}
