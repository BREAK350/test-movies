import { Movie } from '../store/modules/movies';
import { Request } from './withServices';

export const fetchAllMovies = (request: Request<Movie[]>) => () =>
	request({
		method: 'get',
		url: 'http://localhost:4000/movies',
	});

export const getMovie = (request: Request<Movie>) => (movieId: number) =>
	request({
		method: 'get',
		url: `http://localhost:4000/movies/${movieId}`,
	});
