const nextRoutes = require('next-routes');
const routes = module.exports = nextRoutes();

routes.add('home', '/', 'index');
routes.add('about', '/about', 'about');
routes.add('login', '/login', 'login');
routes.add('signup', '/signup', 'signup');
routes.add('library', '/library', 'library');
routes.add('createMovie', '/library/new', 'createMovie');
routes.add('depthMovie', '/movie/:id', 'depthMovie');