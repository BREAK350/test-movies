declare module 'react-tree-walker' {
	export default function reactTreeWalker(app: any, visitor: (element: any, instance: any)): Promise<void>;
}