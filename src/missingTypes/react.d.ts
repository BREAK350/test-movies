import 'react';
import { NextContext } from 'next';
import { AppComponentContext } from 'next/app';

type GetInitialProps = (ctx: NextContext | AppComponentContext) => Promise<any>;

declare module 'react' {
    interface StatelessComponent {
        getInitialProps?: GetInitialProps;
    }

    interface StaticLifecycle {
        getInitialProps?: GetInitialProps;
    }
}
