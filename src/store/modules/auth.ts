export const actionTypes = {
	STORE_USER: 'test/STORE_USER',
};

export type AuthState = null | {
	id: number;
	user: string;
	password: string;
};

const initialState: AuthState = null;

export default function reducer(state = initialState, action: any = {}) {
	switch (action.type) {
		case actionTypes.STORE_USER:
			return action.payload;
		default:
			return state;
	}
}

export function storeUser(user: any) {
	return {
		type: actionTypes.STORE_USER,
		payload: user,
	};
}
