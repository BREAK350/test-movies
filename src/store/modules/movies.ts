export interface Movie {
	id: number;
	userId: number;
	name: string;
	description: string;
	picture: string;
	year: number;
	genre: string;
}
