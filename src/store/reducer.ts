import { combineReducers } from 'redux';

import auth, { AuthState } from './modules/auth';

export default combineReducers({
	auth,
});

export interface State {
	auth: AuthState;
}
