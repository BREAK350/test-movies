export interface DefaultTranslation {
	id: string;
	defaultMessage: string;
}

export interface CachedTranslations {
	[id: string]: string;
}

const cachedTranslations: CachedTranslations = {};

export function registerTranslations(translations: DefaultTranslation[]) {
	translations.forEach(translation => {
		if (translation.id in cachedTranslations) {
			console.warn(`Duplicate of translation id: ${translation.id}`);
		}

		cachedTranslations[translation.id] = translation.defaultMessage;
	});
}

export const getTranslations = () => cachedTranslations;
