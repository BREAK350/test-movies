import React, { ChangeEvent } from 'react';
import { uniq, prop, map, flow, sortBy, identity } from 'lodash/fp';

import { Movie } from '../../store/modules/movies';

import styles from './MovieFilter.scss';

const getYears: (movies: Movie[]) => number[] = flow(
	map(prop('year')),
	uniq,
	sortBy(identity)
);

const getGenres: (movies: Movie[]) => string[] = flow(
	map(prop('genre')),
	uniq,
	sortBy(identity)
);

export interface MovieFilterRenderProps {
	movies: Movie[];
}

interface MovieFilterProps {
	movies: Movie[];
	children: (props: MovieFilterRenderProps) => React.ReactNode;
}

interface MovieFilterState {
	search: string;
	years: { [year: number]: boolean };
	genres: { [genre: string]: boolean };
}

export default class MovieFilter extends React.Component<MovieFilterProps, MovieFilterState> {
	state: MovieFilterState = {
		search: '',
		years: {},
		genres: {},
	};

	translations = [
		{ id: 'movieFilter1', defaultMessage: 'Movie Filter 1' },
		{ id: 'movieFilter2', defaultMessage: 'Movie Filter 2' },
	];

	onChangeSearch = (event: ChangeEvent<{ value: string }>) => {
		this.setState({
			search: event.target.value,
		});
	};

	render() {
		const { children, movies } = this.props;

		const years = getYears(movies);
		const genres = getGenres(movies);

		let filteredMovies = movies;

		if (this.state.search.length > 0) {
			const search = new RegExp(this.state.search, 'i');
			filteredMovies = filteredMovies.filter(movie => search.test(movie.name) || search.test(movie.description));
		}

		if (years.some(year => !!this.state.years[year])) {
			filteredMovies = filteredMovies.filter(movie => !!this.state.years[movie.year]);
		}

		if (genres.some(genre => !!this.state.genres[genre])) {
			filteredMovies = filteredMovies.filter(movie => !!this.state.genres[movie.genre]);
		}

		return (
			<React.Fragment>
				<div className={styles.searchContainer}>
					<input
						type="text"
						className={styles.searchInput}
						value={this.state.search}
						onChange={this.onChangeSearch}
						placeholder="Live search..."
					/>
					<div className={styles.yearFilter}>
						<div className={styles.filterTitle}>Filter by year</div>
						{years.map(year => (
							<label key={year} className={styles.filterItem}>
								<input
									type="checkbox"
									checked={!!this.state.years[year]}
									onChange={() => this.setState(state => ({ years: { ...state.years, [year]: !state.years[year] } }))}
								/>
								{year}
							</label>
						))}
					</div>
					<div className={styles.genreFilter}>
						<div className={styles.filterTitle}>Filter by genre</div>
						{genres.map(genre => (
							<label key={genre} className={styles.filterItem}>
								<input
									type="checkbox"
									checked={!!this.state.genres[genre]}
									onChange={() => this.setState(state => ({ genres: { ...state.genres, [genre]: !state.genres[genre] } }))}
								/>
								{genre}
							</label>
						))}
					</div>
				</div>
				{children({ movies: filteredMovies })}
			</React.Fragment>
		);
	}
}
