import React from 'react';
import { Link } from '../../routes';
import { connect } from 'react-redux';

import { State } from '../../store/reducer';
import { AuthState } from '../../store/modules/auth';

import UserDropdown from './UserDropdown';

import styles from './Header.scss';

export interface HeaderOwnProps {
	route: string;
}

export interface HeaderStateProps {
	auth: AuthState;
}

export interface HeaderProps extends HeaderOwnProps, HeaderStateProps {}

const links = [
	{
		route: '/',
		title: 'Home',
	},
	{
		route: '/about',
		title: 'About',
	},
];

class Header extends React.Component<HeaderProps> {
	translations = [{ id: 'header.home', defaultMessage: 'Home' }, { id: 'header.about', defaultMessage: 'About' }];

	render() {
		const { route, auth } = this.props;

		const logged = !!auth;

		return (
			<div className={styles.headerContainer}>
				<div className={styles.header}>
					{links.map(link => (
						<Link key={link.route} route={link.route}>
							<a className={route === link.route ? styles.activeHeaderItem : styles.headerItem}>{link.title}</a>
						</Link>
					))}
					<div className={styles.user}>
						{logged ? (
							<UserDropdown />
						) : (
							<Link route="/login">
								<a className={route === '/login' ? styles.activeHeaderItem : styles.headerItem}>Login</a>
							</Link>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default connect((state: State) => ({
	auth: state.auth,
}))(Header);
