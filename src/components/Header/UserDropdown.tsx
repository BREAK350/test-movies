import React from 'react';
import { Link, Router } from '../../routes';
import { connect } from 'react-redux';

import { AuthState, storeUser } from '../../store/modules/auth';
import { State } from '../../store/reducer';

import styles from './UserDropdown.scss';

interface UserDropdownStateProps {
	auth: AuthState;
}

interface UserDropdownDispatchProps {
	storeUser: typeof storeUser;
}

interface UserDropdownProps extends UserDropdownStateProps, UserDropdownDispatchProps {}

class UserDropdown extends React.Component<UserDropdownProps> {
	logout = () => {
		this.props.storeUser(null);
		window.localStorage.removeItem('auth');
		Router.pushRoute('/login');
	};

	render() {
		const { auth } = this.props;

		if (!auth) {
			// something went wrong here
			return null;
		}

		return (
			<div className={styles.container}>
				<div className={styles.userName}>{auth.user}</div>
				<div className={styles.dropdown}>
					<Link route="/library">
						<a className={styles.dropdownItem}>Library</a>
					</Link>
					<button className={styles.logout} onClick={this.logout}>
						Logout
					</button>
				</div>
			</div>
		);
	}
}

export default connect(
	(state: State) => ({
		auth: state.auth,
	}),
	{
		storeUser,
	}
)(UserDropdown);
