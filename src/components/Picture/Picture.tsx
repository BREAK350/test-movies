import React from 'react';
import classNames from 'classnames';

import styles from './Picture.scss';

interface PictureProps {
	url?: string;
	className?: string;
	component?: string;
}

export default function Picture({ url, className, component: Component = 'div' }: PictureProps) {
	return <Component style={{ backgroundImage: `url(${url})` }} className={classNames(styles.picture, className)} />;
}
