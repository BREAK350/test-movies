import React from 'react';
import { Form, Field } from 'react-final-form';
import { Omit } from 'type-zoo';

import { Movie } from '../../../store/modules/movies';
import Picture from '../../Picture/Picture';

import styles from './MovieForm.scss';

export type MovieInForm = Omit<Movie, 'id' | 'userId'>;

interface MovieProps {
	// initial value to fill in the form
	value?: MovieInForm;
	// onChange is called when user clicks on save button
	onChange: (value: MovieInForm) => void;
}

export default function MovieForm(props: MovieProps) {
	const { value, onChange } = props;

	return (
		<Form onSubmit={onChange} initialValues={value}>
			{({ handleSubmit, values }) => (
				<form onSubmit={handleSubmit} className={styles.form}>
					<Picture className={styles.picture} url={values.picture} />
					<Field component="input" type="text" name="picture" className={styles.pictureInput} placeholder="Picture url..." />
					<span className={styles.nameLabel}>Name</span>
					<Field component="input" type="text" name="name" className={styles.nameInput} placeholder="Name..." />
					<span className={styles.descriptionLabel}>Description</span>
					<Field
						component="input"
						type="text"
						name="description"
						className={styles.descriptionInput}
						placeholder="Description..."
					/>
					<span className={styles.yearLabel}>Year</span>
					<Field component="input" type="text" name="year" className={styles.yearInput} placeholder="Year..." />
					<span className={styles.genreLabel}>Genre</span>
					<Field component="input" type="text" name="genre" className={styles.genreInput} placeholder="Genre..." />
					<button type="submit" className={styles.save}>
						Save
					</button>
				</form>
			)}
		</Form>
	);
}
