import React from 'react';

import { Movie } from '../../store/modules/movies';

import styles from './MovieTile.scss';

interface MovieTileProps {
	movie: Movie;
}

export default function MovieTile(props: MovieTileProps) {
	return (
		<div className={styles.tile} data-test-id={`tile-${props.movie.name}`}>
			<div className={styles.picture} style={{ backgroundImage: `url(${props.movie.picture})` }} />
			<div className={styles.name}>{props.movie.name}</div>
		</div>
	);
}
