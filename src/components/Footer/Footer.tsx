import React from 'react';

import styles from './Footer.scss';

export default function Footer() {
	return (
		<div className={styles.footer}>
			<span className={styles.author}>
				Created by <b>Dmytro Lymarenko</b> for
			</span>{' '}
			<img src="/static/pointerbpLogo.png" className={styles.logo} />
		</div>
	);
}
