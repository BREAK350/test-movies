import React from 'react';
import { Container, AppComponentProps } from 'next/app';

import { Provider } from 'react-redux';
import { Store } from 'redux';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';

import css from './App.scss';

export interface AppProps extends AppComponentProps {
	store: Store;
}

export default function App(props: AppProps) {
	const { Component, store, router, pageProps } = props;

	return (
		<Container>
			<Provider store={store}>
				<div className={css.app}>
					<Header route={router.route} />
					<div className={css.content}>
						<Component {...pageProps} />
					</div>
					<Footer />
				</div>
			</Provider>
		</Container>
	);
}
