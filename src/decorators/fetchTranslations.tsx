import React from 'react';
import { Store } from 'redux';
import { AppComponentContext, AppComponentProps } from 'next/app';
import reactTreeWalker from 'react-tree-walker';
import AppComponent from '../components/App/App';
import { DefaultTranslation } from '../tools/translations';

export default function fetchTranslations<OwnProps>(App: React.ComponentType<OwnProps & AppComponentProps>) {
	return class FetchTranslations extends React.Component<OwnProps & AppComponentProps> {
		static async getInitialProps(appCtx: AppComponentContext & { ctx: { store: Store } }) {
			console.log('withTranslations', appCtx.ctx.store);
			let appProps: { pageProps: any } = { pageProps: {} };

			if (App.getInitialProps) {
				appProps = await App.getInitialProps(appCtx);
			}

			const translations: DefaultTranslation[] = [];

			try {
				console.time('reactTreeWalker');
				console.time('app');
				const app = <AppComponent {...appProps} Component={appCtx.Component} router={appCtx.router} store={appCtx.ctx.store} />;
				console.timeEnd('app');
				await reactTreeWalker(app, (_, instance) => {
					if (instance && instance.translations) {
						console.log('found translations', instance.translations);
						translations.push(...instance.translations);
					}
				});
				console.timeEnd('reactTreeWalker');
			} catch (err) {}

			return appProps;
		}

		render() {
			return <App {...this.props} />;
		}
	};
}
