import React from 'react';
import hoistReactStatics from 'hoist-non-react-statics';

import { DefaultTranslation } from '../tools/translations';

export interface TranslationsMap {
	[name: string]: DefaultTranslation;
}

export interface WithTranslationsProps {
	translations: { [name: string]: string };
}

export default function withTranslations<OwnProps>(translationMap: TranslationsMap) {
	return (WrappedComponent: React.ComponentType<OwnProps & WithTranslationsProps>) => {
		class WithTranslations extends React.Component<OwnProps> {
			translations: DefaultTranslation[] = Object.values(translationMap);

			render() {
				return (
					<WrappedComponent
						{...this.props}
						translations={Object.keys(translationMap).reduce(
							(res, key) => ({
								...res,
								[key]: translationMap[key].defaultMessage,
							}),
							{}
						)}
					/>
				);
			}
		}

		return hoistReactStatics(WithTranslations, WrappedComponent);
	};
}
