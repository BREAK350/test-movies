import * as React from 'react';
import * as url from 'url';

export type UrlLike = url.UrlObject | url.Url;

export interface Params {
	[param: string]: number | string;
}

export interface LinkState {
	prefetch?: boolean;
	shallow?: boolean;
	scroll?: boolean;
	replace?: boolean;
	onError?(error: any): void;
	route?: string | UrlLike;
	params?: Params;
	passHref?: boolean;
	children: React.ReactElement<any>;
}

export class Link extends React.Component<LinkState> {}

export interface RouterProps {
	pushRoute(
		route: string,
		params?: Params,
		options?: EventChangeOptions,
	): Promise<boolean>;
}

export const Router: RouterProps;