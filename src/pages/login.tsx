import React from 'react';
import { Form, Field } from 'react-final-form';
import { Link, Router } from '../routes';
import request from 'superagent';
import { connect } from 'react-redux';

import { storeUser } from '../store/modules/auth';

import styles from './login.scss';

interface LoginStateProps {}

interface LoginDispatchProps {
	storeUser: (user: LoginFormValues) => any;
}

interface LoginOwnProps {}

interface LoginProps extends LoginOwnProps, LoginStateProps, LoginDispatchProps {}

interface LoginFormValues {
	user?: string;
	password?: string;
}

class Login extends React.Component<LoginProps> {
	login = async (values: LoginFormValues) => {
		if (values.user && values.password) {
			const res = await request.get('http://localhost:4000/users').query(values);

			if (res.body.length === 1) {
				const user = res.body[0];
				window.localStorage.setItem('auth', JSON.stringify(user));
				this.props.storeUser(user);
				Router.pushRoute('/library');
			}
		}
	};

	render() {
		return (
			<div className={styles.container}>
				<Form onSubmit={this.login}>
					{({ handleSubmit }) => (
						<form onSubmit={handleSubmit} className={styles.login}>
							<span className={styles.userLabel}>User</span>
							<Field name="user" type="text" className={styles.userInput} component="input" />
							<span className={styles.passwordLabel}>Password</span>
							<Field name="password" type="password" className={styles.passwordInput} component="input" />
							<div className={styles.buttons}>
								<button type="submit">Login</button>
								<span> or </span>
								<Link route="/signup">
									<a>Sign up</a>
								</Link>
							</div>
						</form>
					)}
				</Form>
			</div>
		);
	}
}

export default connect(
	null,
	{ storeUser }
)(Login);
