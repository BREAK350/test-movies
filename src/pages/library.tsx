import React from 'react';
import { connect } from 'react-redux';
import request from 'superagent';

import { Link } from '../routes';

import { State } from '../store/reducer';
import { AuthState } from '../store/modules/auth';
import { Movie } from '../store/modules/movies';

import MovieFilter from '../components/MovieFilter/MovieFilter';
import MovieTile from '../components/MovieTile/MovieTile';

import styles from './library.scss';

interface LibraryStateProps {
	auth: AuthState;
}

interface LibraryOwnProps {}

interface LibraryProps extends LibraryOwnProps, LibraryStateProps {}

interface LibraryState {
	movies: Movie[];
}

class Libary extends React.Component<LibraryProps, LibraryState> {
	state: LibraryState = {
		movies: [],
	};

	translations = [{ id: 'libraryPage', defaultMessage: 'Library Page' }];

	async componentDidMount() {
		if (this.props.auth) {
			await this.loadMovies(this.props.auth.id);
		}
	}

	async componentDidUpdate(prevProps: LibraryProps) {
		const prevUserId = prevProps.auth && prevProps.auth.id;
		const currUserId = this.props.auth && this.props.auth.id;

		if (prevUserId !== currUserId && currUserId) {
			await this.loadMovies(currUserId);
		}
	}

	async loadMovies(userId: number) {
		const { body: movies } = await request.get('http://localhost:4000/movies').query({ userId });
		this.setState({ movies });
	}

	render() {
		const { auth } = this.props;

		const { movies } = this.state;

		return (
			<React.Fragment>
				{auth ? (
					<React.Fragment>
						<div className={styles.header}>
							<h1>My Library</h1>
							<Link route="/library/new">
								<button className={styles.addNew} data-test-id="addNewMovie">
									Add new Movie
								</button>
							</Link>
						</div>
						<MovieFilter movies={movies}>
							{({ movies }) => (
								<div className={styles.movieTiles}>
									{movies.map(movie => (
										<div className={styles.editTile} key={movie.id}>
											<Link route={`/library/${movie.id}`}>
												<a>
													<MovieTile movie={movie} />
												</a>
											</Link>
											<button
												className={styles.removeTile}
												onClick={event => {
													event.stopPropagation();
													console.log('Remove movie');
												}}
											>
												x
											</button>
										</div>
									))}
								</div>
							)}
						</MovieFilter>
					</React.Fragment>
				) : (
					<React.Fragment>
						To see this page, please{' '}
						<Link route="/login">
							<a>login</a>
						</Link>
						.
					</React.Fragment>
				)}
			</React.Fragment>
		);
	}
}

export default connect<LibraryStateProps, {}, LibraryOwnProps, State>(state => ({
	auth: state.auth,
}))(Libary);
