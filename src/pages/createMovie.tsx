import React from 'react';
import request from 'superagent';
import { connect } from 'react-redux';
import { Router, Link } from '../routes';
import { State } from '../store/reducer';

import MovieForm from '../components/Forms/MovieForm/MovieForm';

interface CreateMovieStateProps {
	userId?: number | null;
}

function CreateMovie(props: CreateMovieStateProps) {
	if (typeof props.userId !== 'number') {
		return (
			<div>
				<Link route="/login">
					<a>Login</a>
				</Link>{' '}
				to create movie
			</div>
		);
	}
	return (
		<React.Fragment>
			<h1>Create movie</h1>
			<MovieForm
				onChange={async value => {
					await request.post('http://localhost:4000/movies').send({ ...value, userId: props.userId });
					Router.pushRoute('/library');
				}}
			/>
		</React.Fragment>
	);
}

export default connect<CreateMovieStateProps>((state: State) => ({
	userId: state.auth && state.auth.id,
}))(CreateMovie);
