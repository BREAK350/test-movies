import React from 'react';
import request from 'superagent';
import { NextContext } from 'next';
import { Movie } from '../store/modules/movies';
import { Link } from '../routes';
import Picture from '../components/Picture/Picture';

import styles from './depthMovie.scss';

interface DepthMovieProps {
	id: number | null;
}

interface DepthMovieState {
	movie: Movie | null;
}

export default class DepthMovie extends React.Component<DepthMovieProps> {
	static async getInitialProps({ query }: NextContext) {
		const id = Array.isArray(query.id) ? query.id[0] : query.id;
		return { id: id ? parseInt(id, 10) : null };
	}

	state: DepthMovieState = {
		movie: null,
	};

	async componentDidMount() {
		if (this.props.id !== null) {
			await this.loadMovie();
		}
	}

	async componentDidUpdate(prevProps: DepthMovieProps) {
		if (prevProps.id !== this.props.id && this.props.id !== null) {
			await this.loadMovie();
		}
	}

	loadMovie = async () => {
		const { body: movie } = await request.get(`http://localhost:4000/movies/${this.props.id}`);
		this.setState({ movie });
	};

	render() {
		const { movie } = this.state;

		if (!movie) {
			return null;
		}

		return (
			<React.Fragment>
				<div className={styles.breadcrumb}>
					<Link route="/">
						<a>Movies</a>
					</Link>
					&nbsp; &gt; &nbsp;
					{movie.name}
				</div>
				<div className={styles.infoContainer}>
					<Picture className={styles.picture} url={movie.picture} />
					<table className={styles.infoSection}>
						<tbody>
							<tr>
								<td>Name</td>
								<td>{movie.name}</td>
							</tr>
							<tr>
								<td>Description</td>
								<td>{movie.description}</td>
							</tr>
							<tr>
								<td>Year</td>
								<td>{movie.year}</td>
							</tr>
							<tr>
								<td>Genre</td>
								<td>{movie.genre}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</React.Fragment>
		);
	}
}
