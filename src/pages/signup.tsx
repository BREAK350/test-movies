import React from 'react';
import { Form, Field } from 'react-final-form';
import request from 'superagent';
import { Router } from '../routes';

import styles from './signup.scss';

interface SignUpFormValues {
	user?: string;
	password?: string;
	passwordRepeat?: string;
}

export default class Signup extends React.Component {
	signUp = async (values: SignUpFormValues) => {
		await request.post('http://localhost:4000/users').send({
			user: values.user,
			password: values.password,
		});
		Router.pushRoute('/login');
	};

	render() {
		return (
			<div className={styles.container}>
				<Form onSubmit={this.signUp}>
					{({ handleSubmit }) => (
						<form onSubmit={handleSubmit} className={styles.signup}>
							<span className={styles.userLabel}>User</span>
							<Field name="user" type="text" className={styles.userInput} component="input" />
							<span className={styles.passwordLabel}>Password</span>
							<Field name="password" type="password" className={styles.passwordInput} component="input" />
							<span className={styles.passwordRepeatLabel}>Repeat password</span>
							<Field name="passwordRepeat" type="password" className={styles.passwordRepeatInput} component="input" />
							<div className={styles.buttons}>
								<button type="submit">Signup</button>
							</div>
						</form>
					)}
				</Form>
			</div>
		);
	}
}
