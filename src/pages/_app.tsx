import App, { AppComponentContext } from 'next/app';
import React from 'react';
import withRedux from 'next-redux-wrapper';
import request from 'superagent';

import AppComponent, { AppProps } from '../components/App/App';

import initStore from '../store/initStore';
import { storeUser } from '../store/modules/auth';

import fetchTranslations from '../decorators/fetchTranslations';

import './global.scss';

class MoviesApp extends App<AppProps> {
	static async getInitialProps(appCtx: AppComponentContext) {
		const { Component, ctx } = appCtx;

		let pageProps = {};

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx);
		}

		return { pageProps };
	}

	async componentDidMount() {
		const auth = window.localStorage.getItem('auth');

		if (auth) {
			try {
				const user = JSON.parse(auth);
				// check if stored user exists
				const res = await request.get('http://localhost:4000/users').query(user);
				if (res.body.length === 1) {
					this.props.store.dispatch(storeUser(res.body[0]));
				}
			} catch (error) {}
		}
	}

	render() {
		return <AppComponent {...this.props} />;
	}
}

export default withRedux(initStore)(fetchTranslations(MoviesApp));
