import React from 'react';

import styles from './about.scss';

export default function About() {
	return (
		<div className={styles.about}>
			<h1>Frontend Programming Assignment</h1>
			<p>
				Do a simple single page application (in React) that will allow multiple users to log in and add their favorite series or
				movies.
			</p>
			<p>There should be:</p>
			<ul>
				<li>Single Page Application in React (using Redux)</li>
				<li>Login page</li>
				<li>TV/Movies page where you can search and add new entries</li>
				<li>Tests (optional)</li>
			</ul>
			<p>
				User credentials, and other data, can be stored in local storage. Feel free to use tools such as "json-server" for API
				mockups.
			</p>
		</div>
	);
}
