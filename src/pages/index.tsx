import React from 'react';

import { Link } from '../routes';

import { fetchAllMovies } from '../services/movies';
import withServices, { WithServicesProps, NextContextWithServices } from '../services/withServices';

import { Movie } from '../store/modules/movies';

import MovieFilter from '../components/MovieFilter/MovieFilter';
import MovieTile from '../components/MovieTile/MovieTile';
import withTranslations, { WithTranslationsProps } from '../decorators/withTranslations';

import styles from './index.scss';

const services = {
	fetchAllMovies,
};

interface HomeState {
	movies: Movie[];
}

interface HomeOwnProps {
	movies: Movie[];
}

interface HomeProps extends HomeOwnProps, WithServicesProps<typeof services>, WithTranslationsProps {}

class Home extends React.Component<HomeProps, HomeState> {
	static async getInitialProps(ctx: NextContextWithServices<typeof services>) {
		const movies = await ctx.services.fetchAllMovies();

		return {
			movies,
		};
	}

	state: HomeState = {
		movies: this.props.movies,
	};

	render() {
		const { movies } = this.state;

		return (
			<div className={styles.container}>
				<MovieFilter movies={movies}>
					{({ movies }) => (
						<div className={styles.movieTiles}>
							{movies.map(movie => (
								<div className={styles.tile} key={movie.id}>
									<Link route={`/movie/${movie.id}`}>
										<a>
											<MovieTile movie={movie} />
										</a>
									</Link>
								</div>
							))}
						</div>
					)}
				</MovieFilter>
			</div>
		);
	}
}

export default withServices<HomeOwnProps, typeof services>(services)(
	withTranslations<HomeOwnProps & WithServicesProps<typeof services>>({
		homePage: {
			id: 'homePage',
			defaultMessage: 'Home Page',
		},
	})(Home)
);
