const withTypescript = require('@zeit/next-typescript');
const withSass = require('@zeit/next-sass');

module.exports = withSass(withTypescript({
	distDir: '../build',
	cssModules: true,
	useFileSystemPublicRoutes: false,
	cssLoaderOptions: {
		localIdentName: "[local]___[hash:base64:5]",
	}
}));
