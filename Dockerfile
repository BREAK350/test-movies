# base image
FROM node:10.9.0

# set working directory
RUN mkdir /home/app
WORKDIR /home/app

COPY .babelrc .babelrc
COPY next.config.js next.config.js
COPY tsconfig.json tsconfig.json
COPY package.json package.json
COPY yarn.lock yarn.lock

COPY missingTypes.d.ts missingTypes.d.ts

# install dependencies
RUN yarn

COPY src src

# copy json file for json-server
COPY server/db.json server/db.json

# build app
RUN yarn build

# start app
CMD ["yarn", "start"]
