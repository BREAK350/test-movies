# Frontend Programming Assignment

Do a simple single page application (in React) that will allow multiple users to log in and add their favorite series or movies.

There should be:

- Single Page Application in React (using Redux)
- Login page
- TV/Movies page where you can search and add new entries
- Tests (optional)

User credentials, and other data, can be stored in local storage. Feel free to use tools such as "json-server" for API mockups.

## Getting Started

To run the project in dev mode:

```
yarn dev
```

This command will run json-server and the project.

To start the project in production mode first build it:

```
yarn build
```

and then start:

```
yarn start
```

To open the poject after it was built (dev or prod mode) open http://localhost:3000

## Running the tests

To run e2e tests visually use the next command:

```
yarn test:e2e:visual
```
